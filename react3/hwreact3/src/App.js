import React from 'react';
import './style/style.sass';
import {ListOfProduct} from './components/ListofProduct';
import Main from './components/MainMenu';
import {Favourite, HeaderCart} from './components/pages/index'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
function App() {
  return (
    <div className="App">
<Router>
    <Main/>
<Switch>
  <Route exact path='/' component={ListOfProduct}/>
  <Route exact path='/fav' component={Favourite}/>
  <Route exact path='/cart' component={HeaderCart}/>

</Switch>

</Router>
    
    </div>
  );
}

export default App;
