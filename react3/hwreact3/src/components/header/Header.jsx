import React from 'react';

import {NavLink} from 'react-router-dom'

export const Header = () => {
    <div>

<NavLink to='/'>Home</NavLink>
        <NavLink to='/cart'>Cart</NavLink>
        <NavLink to='/fav'>Favourite</NavLink>

    </div>
  
}