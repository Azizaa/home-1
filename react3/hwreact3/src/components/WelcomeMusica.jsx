import React, {Component} from 'react'
import '../style/style.sass'
import cd from '../images/img/Icons/feature-icon/cd.png'
import  headphone from '../images/img/Icons/feature-icon//headphone.png'
import calendar from '../images/img/Icons/feature-icon/calendar.png'
import line from '../images/img/line.png' 

export class MainSection extends Component {
  render(){


return (


<div >
<section className="musica">
<div className="container">
  <h1 className="welcome-text">WELCOME TO <span>MUSICA,</span> CHECK OUR LATEST ALBUMS</h1>
<img src={line} alt="" />
  <div className="welcome-to-musica">
  <div className="choices">
  <h3><img src={cd}alt="" /> 
    CHECK OUR CD COLLECTION</h3>
    <p>Donec pede justo, fringilla vel, al, vulputate  eget, arcu. In </p>
  </div>

  <div className="choices">
  <h3><img src={headphone}alt="" />  
    LISTEN BEFORE PURCHASE</h3>
    <p>Donec pede justo, fringilla vel, al, vulputate  eget, arcu. In </p>
  </div>

  <div className="choices">
  <h3><img src={calendar} alt="" /> 
    UPCOMING EVENTS</h3>
    <p>Donec pede justo, fringilla vel, al, vulputate  eget, arcu. In </p>
   </div>
   </div>
</div>
</section>
</div>
)
}
}