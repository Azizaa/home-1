import React from 'react';
import '../style/style.sass'
const useStyles = makeStyles({
  root: {
    maxWidth: 270,
  },
  media: {
    height: 160,
  },
});

export default function MediaCard() {
  
  const classes = useStyles();

  return (
  
    <Card className={classes.root}>
        
      <CardActionArea> 
        <CardMedia
          className={classes.media}
          image="./../"
          title="Deer"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            Oh my Deer
          </Typography>
          <Typography variant="body2" color="text2" component="p">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Impedit quo fugit officia adipisci iure fuga 
            facilis quasi deserunt, 
            
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <h3>$14.99</h3>
        <Button class="btn" size="small" >
          Add to cart
        </Button>
      </CardActions>
    </Card>
    
  );
}