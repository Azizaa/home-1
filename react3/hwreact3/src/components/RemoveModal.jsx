import React from "react";
import { useState } from "react";

export const RemoveModal = props => {
 
  console.log("removemodal");
  const {
  
    header,
    closeIcon,
    toggle,
    text,
    okHandle,
    cancelHandle
  } = props;
  return (
    
    <div className="bk-ground">
      <div
        className="modal_window"
        onClick={e => {
          e.stopPropagation();
        }}
      >
        <div className="modal-header-container">
          <header className="modal-header">{header}</header>
          {closeIcon && (
            <button
              className="close-btn"
              onClick={toggle}
              className="close-btn"
            >
              x
            </button>
          )}
        </div>
        <p className="main-text">{text}</p>
        <div className="btn-container">
          <button onClick={okHandle}>OK</button>
          <button onClick={cancelHandle}>Cancel</button>
        </div>
      </div>
    </div>
  );
};
