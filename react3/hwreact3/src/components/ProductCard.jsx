import React, { Component } from "react";
import ReactStars from 'react-rating-stars-component';


export class ProductCard extends Component {
  ratingChanged = newRating => {
    console.log(newRating);
  };
  render() {
    const { src, title, artist, description, price, handleClick } = this.props;
    return (
      <div>
        <div className="">
          <img src={src} alt="" />
          <div className="">
            <div className="">
              {title}
              <span>
                <i> {artist}</i>
              </span>
            </div>
             <ReactStars
              count={5}
              onChange={this.ratingChanged}
              size={24}
              color2={"#d23939"}
            /> 
          </div>
          
          <p className="product-cnt">{description}</p>
          <div className="cart-price-btn cnt">
         <span>${price}</span> <button onClick={handleClick}>ADD TO CART</button>
        </div>
        </div>
        
      </div>
    );
  }
}
