import React,{Component} from 'react';
import '../style/style.sass'
import facebook from '../images/img/Icons/facebook.png'
import  dribble from '../images/img/Icons/dribble.png';
import twitter from '../images/img/Icons/twitter.png';
import vimeo from '../images/img/Icons/vimeo.png';
import mail from '../images/img/Icons/mail.png';
import card from '../images/img/Icons/card.png';
import logo from '../images/img/logo.png';
import {NavLink} from 'react-router-dom'

 export default class Main extends Component {

 render() {

  return (
    <header className="header">
      <div className="first-header-line">
    <div className="navbar">
      <div className="navbar1">
    <div className="icon">
        <img src={facebook} alt="fb"/></div>
      
        <div className="icon">
        <img src={dribble} alt="dribble" /></div>
      
      
      <div className="icon">
        <img src={twitter} alt="twt" />
        </div>
        
      
      <div className="icon">
        <img src={mail} alt="mail" />
        </div>
        
      
      <div className="icon">
        <img src={vimeo} alt="vimeo" />
        </div>
<div className="login-cart">
  <h3 className = "login-register">Login/Register</h3>
  <button className="cart">
    <img src={card} alt="logo-card" />
    Cart
  </button>
</div>
        </div>
   
      </div>
</div>
<div className="navbar">
  <div className="second-header-line">

      <img src={logo} alt="logo"/>
     
        <NavLink to='/'>Home</NavLink>
        <NavLink to='/cart'>Cart</NavLink>
        <NavLink to='/fav'>Favourite</NavLink> 

          </div>
  </div>
</header>
 
 );
}
  
}




