import React, { useState, useEffect } from "react";
import { RemoveModal } from "../RemoveModal";

export const HeaderCart = props => {
  const [cart, setCart] = useState([]);
  const [active, setisActive] = useState(false);
  const [toBeDeleted, setToBeDeleted] = useState();

  const getData = () => {
    if (!!localStorage.length) {
      setCart([...JSON.parse(localStorage.getItem("cart"))]);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    localStorage.setItem("Product cart", JSON.stringify(...cart));
    return () => console.log("Saved");
  });

  const handleCLick = (id, count) => {
    setToBeDeleted({ id, count });
    setisActive(true);
    console.log(active)
  };


  const removeFromHeaderCart = ({ id, count }) => {
    if (count > 1) {
      setCart(cart =>
        cart.map(item => {
          if (item.id === id) {
            return {
              ...item,
              count: item.count - 1
            };
          }
          return item;
        })
      );
    } else {
      setCart(cart => cart.filter(product => product.id !== id));
    }
    setisActive(false);
    setToBeDeleted({});
  };

  return (
    <div>
      {active && (
        <RemoveModal
          key={1}
          header="Do you want to delete product from card?"
          text="Are you sure?"
          closeIcon={true}
          toggle={() => setisActive(false)}
          okHandle={() => removeFromHeaderCart({ ...toBeDeleted })}
          cancelHandle={() => setisActive(false)}
        />
      )}
      <div className="checkout-card cnt">
        {cart.map(({ id, title, cost, count }) => (
          <div key={id} className="checkout-item">
            <h4>{title}</h4>
            <p>Price: ${cost}</p>
            <p>Count:  {count}</p>

            <button onClick={() => handleCLick(id, count)}>X</button>
          </div>
        ))}
        <div className="total">
           <b>Total Delivery cost:</b> 
           <div className="total-price">
             ${ cart.reduce((total, { cost, count }) =>(parseFloat(total) + cost * count).toFixed(2), 0)}
             </div>
             </div>
            
      </div>
    </div>
  );
};
