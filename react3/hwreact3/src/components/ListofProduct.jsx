/* eslint-disable no-unused-vars */
import React, { useState,useEffect } from "react";
import '../style/style.sass'
import { ProductCard } from "./ProductCard";
import { Cart } from "./Cart";
import { getMusics } from "../API/fetchAPI";
import ReactStars from 'react-rating-stars-component'

export const ListOfProduct = props => {
  const [cart, setCart] = useState([]);
  const [data, setData] = useState({
    count: 0,
    musics: []
  });
  console.log(data.musics, "musics");

  const getData = async () => {
    const answer = await getMusics();
    setData(data => ({
      ...answer,
      musics: [...data.musics, ...answer]
    }));
  };

  useEffect(() => {
    getData();
  }, []);

  console.log(cart, "shopping cart");
  const AddToCart = (imgPath, title, rate, cost, id) => {
    const isAvailable = cart.find(item => id === item.id);
    if (isAvailable) {
      setCart(cart =>
        cart.map(item => {
          if (item.id === id) {
            return { ...item, count: item.count + 1 };
          }
          return item;
        })
      );
    } else {
      setCart(cart => [
        ...cart,
        {
          imgPath,
          title,
          rate,
          cost,
          id,
          count: 1
        }
      ]);
    }
  };
  localStorage.setItem("cart", JSON.stringify(cart));

  return (
    <div>
      <section className="arrivals">
        <div className="container">
          <div className="arrivals-header">
            <h1>LATEST ARRIVALS IN MUSICA</h1>
            <img src={process.env.PUBLIC_URL + "/images/line.png"} alt="" />
            <div className="buttons">
              <button className="arrows-btn">
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/arrows/left.png"}
                  alt=""
                />
              </button>
              <button className="arrows-btn">
                <img
                  src={
                    process.env.PUBLIC_URL + "/images/Icons/arrows/right.png"
                  }
                  alt=""
                />
              </button>
            </div>
          </div>

          <ul className="product-list">
            {data.musics.map(
              ({ imgPath, id, title, rate, price, description, artist }) => (
                <li key={id} className="products-item">
                  <ProductCard
                    key={id}
                    src={imgPath}
                    title={title}
                    artist={artist}
                    description={description}
                    price={price}
                    handleClick={() =>
                      AddToCart(imgPath, title, rate, price, id)
                    }
                  />
                </li>
              )
            )}
          </ul>
        </div>
      </section>
    </div>
  );
};
