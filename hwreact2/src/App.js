import React from 'react';
import './style/style.sass';
import {ListOfProduct} from './component/ListofProduct';
import Main from './component/MainMenu';
function App() {
  return (
    <div className="App">
    <Main/>
    <ListOfProduct/>
    </div>
  );
}

export default App;
