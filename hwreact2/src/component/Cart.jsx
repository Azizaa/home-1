import React, { Component } from 'react';

import ReactStars from 'react-rating-stars-component';





export class Cart extends Component {
  constructor(props){
      super(props);
    this.state = {
    cart: []
    }
    
  
  }

  render(){
   
      return (
          <div className="modal_window" >  
          {  this.props.cart.map(({imgPath, title,rate, cost, count,id }) => (
                <div key= {id} className="about-product">
                 <div className="count-title"> 
                  <img src={imgPath} className ="product-img" alt = ""/>
              <div className="about_cart">
           
             <h3>{count} x {title}
              <ReactStars
            count={5}
            onChange={this.ratingChanged}
            size={24}
            color2={"#d23939"}
          /> 
          
             </h3>
           
             
            <div className="price-remove-btn">
             <p>${cost}</p>
              <button className="modal-remove-btn" onClick={()=>this.props.removeFromCart(id,count)}>x</button>
            </div>
          
            </div>  
             </div>
             </div>
             ))}
             <div className="total">
            <b>Total Delivery cost:</b> 
            <div className="total-price">
              ${ this.props.cart.reduce((total, { cost, count }) =>(parseFloat(total) + cost * count).toFixed(2), 0)}
              </div>
              </div>
              <div className="view-card-proceed">
                <button className="view-card-btn">View Card &#8594;</button>
                <button className="proceed-btn">Proceed to Checkout &#8594;</button>
                </div>
             </div>
      )
  }
}
