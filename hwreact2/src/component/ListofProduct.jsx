/* eslint-disable no-unused-vars */
import React, { Component } from "react";
import '../style/style.sass'
import { ProductCard } from "./ProductCard";
import { Cart } from "./Cart";
import { getMusics } from "../API/fetchAPI";
import ReactStars from 'react-rating-stars-component'
export class ListOfProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {            
      data: {
        count: 0,
        musics: []
      },
      cart: [],
      isactive: false
    };
  }

  getdata = async () => {
    const answer = await getMusics();
    this.setState({ data: { musics: [...answer] } });
    console.log(this.state.data.musics);
  };

  componentDidMount() {
    this.getdata();
  }

  removeFromCart = (id, count) => {
  
    if (count > 1) {
        this.setState({cart : this.state.cart.map(item => {
            if (item.id === id) {
                return {
                    ...item,
                    count: item.count - 1
                }
            }
            return item
        })})
    } else {
        this.setState({cart : this.state.cart.filter(item => item.id !== id)})
    }
}

  AddToCart = (imgPath, title, rate, cost, id) => {
    console.log(this.state.cart,"Add to cart begin");
    
    const isAvailable = this.state.cart.find(item => id === item.id);
    if (isAvailable) {
      this.setState(
        {  cart:
        this.state.cart.map(item => {
          if (item.id === id) { 
           
            console.log({...item, count: item.count + 1},"Inside if isavailable");
            return {...item, count: item.count + 1};
          }
          return item;
        })
      }
      );
   
    } else {
      this.setState({
        cart: [
          ...this.state.cart,
          {
            imgPath,
            title,
            rate,
            cost,
            id,
            count: 1
          }
        ]
      });
    }
    this.setState({ isactive: true });
  };
 
  render() {
    console.log(process.env.PUBLIC_URL);
    console.log(this.state.cart);
    return (
      <section className="arrivals">
        <div className="container">
          <div className="arrivals-header">
            <h1>LATEST ARRIVALS IN MUSICA</h1>
            <img src= { process.env.PUBLIC_URL +"/images/line.png" } alt="" />
            <div className="buttons">
              <button className="arrows-btn">
                <img
                  src={process.env.PUBLIC_URL + "/images/Icon/arrows/left.png"}
                  alt=""
                />
              </button>
              <button className="arrows-btn">
                <img
                  src={ process.env.PUBLIC_URL + "/images/Icon/arrows/right.png"}
                  alt=""
                />
              </button>
            </div>
          </div>
          {
             this.state.isactive  &&  
                    <Cart
                     
                      cart={this.state.cart}
                      removeFromCart={this.removeFromCart}
                    />
              
              }
          <ul className="product-list">
            {this.state.data.musics.map(
              ({ imgPath, id, title, rate, price, description, artist }) => (
                <li key={id} className="products-item">
                  <ProductCard
                    key={id}
                    isactive={this.props.activetab} 
                    src={imgPath}
                    title={title}
                    artist={artist}
                    description={description}
                    price={price}
                    handleClick={() =>
                      this.AddToCart(imgPath, title, rate, price, id)
                    }
                    
                  />
 
   
                </li>
              )
            )}
          </ul>
        </div>
      </section>
    );
  }
}
