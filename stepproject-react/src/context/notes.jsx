import React, {createContext, useState, useEffect} from 'react';

export const NoteContext = createContext();

export const NoteContextProvider = ({children}) => {

    const [notes, setNotes] = useState([]);

    const getData = async() => {
      const response = await fetch(' http://localhost:3000/notes');
      const newData = await response.json();
      console.log(newData);
      setNotes(newData);
    }

    useEffect(() => {
      getData();
    }, []);



    const addNote = note => {
      setNotes(prev =>[...prev, {...note}])
    };

    const deleteNote = id => {
      setNotes(note => note.filter(item => item.id !== id))
    }


      return(
        <NoteContext.Provider value = {{notes, addNote, deleteNote}}>
            {children}
        </NoteContext.Provider>
    )
  }
  
