import React from 'react';
import { Homepage, Create } from "./pages";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {Header} from './commons';
import {NoteContextProvider} from './context/notes';


function App() {
  return (
    <NoteContextProvider>
      <Router>
        <Header />
        <Switch>
          <Route path = "/" exact component = {Homepage} />
          <Route path = "/create" exact component = {Create} />
        </Switch>
      </Router>
    </NoteContextProvider>
    
  );
}

export default App;
