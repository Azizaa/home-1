import React, {Component} from 'react' ;
import PropTypes from 'prop-types' ;
export class Modal extends Component {
     
    render() {

        return (
            <div className="bckground">
            <div  className="modal" onClick={(e) => {e.stopPropagation();}}>
              <div style={{backgroundColor: this.props.backgroundColor}} className="header-bckground">
                <header className="header" >{this.props.header}
                </header>
                
              {this.props.closeIcon &&  <button  className="close-btn" onClick={this.props.remove}>x</button>}
              </div>
              <p className="modal-text">{this.props.text}</p>
              {this.props.action}
  
        </div>
        </div>
        
      );
    }
  }
  Modal.propTypes = {
    backgroundColor: PropTypes.string,
    header: PropTypes.string,
    closeIcon: PropTypes.bool,
    toggle: PropTypes.func,
    text: PropTypes.string,
    action: PropTypes.array
  }
