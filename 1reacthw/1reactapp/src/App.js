import React, { Component } from 'react';
import './App.css';
import './styles.scss'  ;
import { Button }  from  './components/Button';
import {Modal} from './components/Modal';

export class App extends Component{
  constructor(){
      super()
      this.state = {

       firstIsActive: false ,
       secondIsActive: false 
      }
  
  }

setFirstIsActive = () => {
  this.setState({firstIsActive: !this.state.firstIsActive})
}

setScondIsActive = () => {
  this.setState({secondIsActive: !this.state.secondIsActive})
}
  
render () 
{

  return (
      <div className='modal1'>
      <div onClick={this.setFirstIsActive}>
      {this.state.firstIsActive ? 
      <Modal
            header="Do you want to delete this file? " 
            closeIcon={true}
            color = "000000"
            remove={this.setFirstIsActive}
            text="Once you delete this file, it won’t be possible to undo this action. 
             Are you sure you want to delete it?"   
             action ={[ <Button className = "delete-btn"

                      text="Ok" 
                      backgroundColor="#b3382c" 
                      onClick={this.setFirstIsActive}/>,
                      
                      <Button className = "confirm-btn"
                      text="Cancel" 
                      backgroundColor="#b3382c" 
                      onClick={this.setFirstIsActive}/>]} /> : null}</div>
                                                         

        <div onClick={this.setScondIsActive}>
          {this.state.secondIsActive ? 
           <Modal
           
           header="Are you sure to confirm ? " 
           remove={this.setScondIsActive}
           backgroundColor="#b3382c"
           closeIcon={true}

           text="Are you sure you want to proceed with this process ? 
           It will be impossible to redo this step after"
           action ={[ 
            
           <Button className = "delete-btn"
                      text="Okay" 
                      backgroundColor="#b3382c" 
                      onClick={this.setScondIsActive}/>,
            <Button className = "confirm-btn"
                      text="Cancel" 
                      backgroundColor="#b3382c" 
                      onClick={this.setScondIsActive}/>]}/>: null }</div>
               
          
      <Button text="Open first modal " backgroundColor="#000000" onClick={this.setFirstIsActive}/>
      <Button text="Open second modal " backgroundColor="#000000" onClick={this.setScondIsActive}/>
          
          
      </div>
  )
}

}
export default  App ;
 